# Morra - Rock Paper Scissor

A simple Rock Paper and Scissor game done with angular

<img align="left" width="50%" height="100%" alt="rock-paper-scissor" src="https://gitlab.com/moio/angular-morra/-/raw/master/rock-paper-scissor.gif">

<img align="right" width="50%" height="100%" alt="spock-lizard" src="https://gitlab.com/moio/angular-morra/-/raw/master/lizard-spock.gif">

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
