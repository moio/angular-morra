import { Injectable } from '@angular/core';
import { Weapon } from '../models/weapon';
import { Winner } from '../models/winner';

@Injectable({
  providedIn: 'root',
})

export class WeaponService {
  weapons: Weapon[] = [Weapon.ROCK, Weapon.PAPER, Weapon.SCISSOR, Weapon.SPOCK, Weapon.LIZARD];

  constructor() {}

  getWeapon(index: number) {
    return this.weapons[index];
  }

  getWinner(weapon1: Weapon, weapon2: Weapon): Winner {
    if (weapon1 === weapon2) return Winner.Tie;
    let w1 = this.weapons.indexOf(weapon1);
    let w2 = this.weapons.indexOf(weapon2);
    let result = (this.weapons.length + w1 - w2) % this.weapons.length;

    if (result % 2 === 0) return Winner.Player2;
    else return Winner.Player1;
  }

}
