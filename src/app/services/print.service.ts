import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  constructor() { }

  private whoWin(score: number) {
    let winner = '';
    if (score < 0) {
      winner = 'Player 1';
    } else if (score > 0) {
      winner = 'Player 2';
    } else {
      winner = 'No one';
    }
    return winner;

  }

  gameWinner(score: number) {
    return this.whoWin(score) + ' wins the game!';
  }

  roundWinner(roundScore: number) {
    return this.whoWin(roundScore);
  }

}
