import { Injectable } from '@angular/core';
import { Weapon } from '../models/weapon';
import { Player } from '../models/player';

@Injectable({
  providedIn: 'root',
})
export class PlayerService implements Player {
  weapon!: Weapon;

  chooseWeapon(weapon: Weapon) {
    this.weapon = weapon;
  }
}
