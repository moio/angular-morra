import { TestBed } from '@angular/core/testing';
import {Weapon} from '../models/weapon';

import { BotService } from './bot.service';
import { WeaponService } from './weapon.service';

describe('BotService', () => {
  let service: BotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should choose a the selected weapon', () => {
    service.chooseWeapon(Weapon.ROCK);
    expect(service.weapon).toBe(Weapon.ROCK);
  });

  it('should choose a valid random weapon', () => {
    const weapons = new WeaponService().weapons;
    service.chooseWeapon(Weapon.RANDOM);
    expect(weapons).toContain(service.weapon);
  });

  it('random weapon should change', () => {
    let randSpy = spyOn(Math, 'random').and.returnValue(0);
    service.chooseWeapon(Weapon.RANDOM);
    expect(service.weapon).toContain(Weapon.ROCK);
    randSpy.and.returnValue(0.9);
    service.chooseWeapon(Weapon.RANDOM);
    expect(service.weapon).toContain(Weapon.SCISSOR);
  });
});
