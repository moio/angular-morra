import { TestBed } from '@angular/core/testing';

import { WeaponHelperService } from './weapon-helper.service';

describe('WeaponHelperService', () => {
  let service: WeaponHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeaponHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
