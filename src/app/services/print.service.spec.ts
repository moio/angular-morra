import { TestBed } from '@angular/core/testing';

import { PrintService } from './print.service';

describe('PrintService', () => {
  let service: PrintService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrintService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('return player1 on a negative score', () => {
    expect(service.roundWinner(-4)).toBe('Player 1');
  });

  it('return player2 on a positive score', () => {
    expect(service.roundWinner(4)).toBe('Player 2');
  });

  it('return no one on a zero score', () => {
    expect(service.roundWinner(0)).toBe('No one');
  });

});
