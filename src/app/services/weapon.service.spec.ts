import { TestBed } from '@angular/core/testing';
import { Weapon } from '../models/weapon';
import { Winner } from '../models/winner';
import { WeaponService } from './weapon.service';

describe('WeaponService', () => {
  let service: WeaponService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeaponService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('return ties for equal weapons', () => {
    expect(service.getWinner(Weapon.ROCK, Weapon.ROCK)).toBe(Winner.Tie);
    expect(service.getWinner(Weapon.PAPER, Weapon.PAPER)).toBe(Winner.Tie);
  });

  it('return right winner', () => {
    expect(service.getWinner(Weapon.ROCK, Weapon.SCISSOR)).toBe(Winner.Player1);
    expect(service.getWinner(Weapon.SCISSOR, Weapon.PAPER)).toBe(
      Winner.Player1
    );
    expect(service.getWinner(Weapon.ROCK, Weapon.SCISSOR)).toBe(Winner.Player1);
    expect(service.getWinner(Weapon.SCISSOR, Weapon.ROCK)).toBe(Winner.Player2);
    expect(service.getWinner(Weapon.ROCK, Weapon.PAPER)).toBe(Winner.Player2);
  });
});
