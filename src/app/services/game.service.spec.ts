// @ts-nocheck
import { TestBed } from '@angular/core/testing';
import {Player} from '../models/player';
import { Weapon } from '../models/weapon';
import { Winner } from '../models/winner';
import {BotService} from './bot.service';
import { GameService } from './game.service';
import {PlayerService} from './player.service';
import { PrintService } from './print.service';
import {WeaponService} from './weapon.service';

describe('GameService', () => {
  let service: GameService;

  let printStub = {
    roundWinner: () => 'Someone',
    gameWinner: () => 'Someone',
  };

  let player: Player;
  let bot: Player;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: PrintService, useValue: printStub }],
    });
    service = TestBed.inject(GameService);
    player = new PlayerService();
    bot = new BotService(new WeaponService());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('update score correctly', () => {
    expect(service.score).toBe(0);
    service.updateGameScore(1);
    expect(service.score).toBe(1);
    service.updateGameScore(-3);
    expect(service.score).toBe(-2);
    service.updateGameScore(1);
    expect(service.score).toBe(-1);
  });

  it('get right winner', () => {
    player.weapon = Weapon.ROCK;
    bot.weapon = Weapon.SCISSOR;
    let winner = service.getRoundWinner(player, bot);
    expect(winner).toBe(Winner.Player1);
  });

  it('plays a round on 1st and 2nd round', () => {
    spyOn(service, 'getRoundWinner');
    spyOn(service, 'updateGameScore');
    spyOn(printStub, 'roundWinner');
    spyOn(printStub, 'gameWinner');
    for (let r = 1; r < 3; r++) {
      service.round = r;
      service.play(player, bot);
      expect(service.getRoundWinner).toHaveBeenCalled();
      expect(service.updateGameScore).toHaveBeenCalled();
      expect(printStub.gameWinner).not.toHaveBeenCalled();
      expect(printStub.roundWinner).toHaveBeenCalled();
    }
  });

  it('game finishes on round 3', () => {
    spyOn(service, 'getRoundWinner');
    spyOn(service, 'updateGameScore');
    spyOn(printStub, 'roundWinner');
    spyOn(printStub, 'gameWinner');
    service.round = 3;
    service.play(player, bot);
    expect(service.getRoundWinner).toHaveBeenCalled();
    expect(service.updateGameScore).toHaveBeenCalled();
    expect(printStub.gameWinner).toHaveBeenCalled();
    expect(printStub.roundWinner).toHaveBeenCalled();
  });

  it('round is incremented modulo of 3 after play', () => {
    service.round = 1;
    service.play(player, bot);
    expect(service.round).toBe(2);
    service.play(player, bot);
    expect(service.round).toBe(3);
    service.play(player, bot);
    expect(service.round).toBe(1);
  });

  it('round reset after round 3', () => {
    service.round = 3;
    spyOn(window, 'alert');
    service.play(player, bot);
    expect(service.round).toBe(1);
    expect(window.alert).toHaveBeenCalled();
  });
});
