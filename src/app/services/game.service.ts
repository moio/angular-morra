import { Injectable } from '@angular/core';
import { PrintService } from './print.service';
import { WeaponService } from './weapon.service';
import { Player } from '../models/player';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  round = 1;
  private score: number = 0;
  roundWinner: string = '';
  gameWinner: string = '';

  constructor(public weapon: WeaponService, private print: PrintService) {}

  private getRoundWinner(player: Player, opponent: Player) {
    return this.weapon.getWinner(player.weapon, opponent.weapon);
  }

  private updateGameScore(roundScore: number) {
    this.score += roundScore;
  }

  public play(player: Player, opponent: Player) {
    this.gameWinner = '';
    let roundScore = 1;
    roundScore = this.getRoundWinner(player, opponent);
    this.updateGameScore(roundScore);
    this.roundWinner = this.print.roundWinner(roundScore);
    this.round++;

    if (this.round > 3) {
      alert(this.print.gameWinner(this.score));
      this.round = 1;
    }
  }
}
