import { TestBed } from '@angular/core/testing';
import { Weapon } from '../models/weapon';
import { PlayerService } from './player.service';

describe('PlayerService', () => {
  let service: PlayerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('chose a weapon', () => {
    let weaponToChoose = Weapon.ROCK as Weapon;
    service.chooseWeapon(weaponToChoose);
    expect(service.weapon).toBe(weaponToChoose);
  });
});
