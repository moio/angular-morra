import { Injectable } from '@angular/core';
import {Weapon} from '../models/weapon';
import {PlayerService} from './player.service';
import {WeaponHelperService} from './weapon-helper.service';

@Injectable({
  providedIn: 'root'
})
export class BotService extends PlayerService {
  weapon!: Weapon;

  constructor() {
    super();
  }

  chooseWeapon(weapon: Weapon) {
    if(weapon === Weapon.RANDOM) {
      this.weapon = WeaponHelperService.getRandomWeapon();
    } else {
      super.chooseWeapon(weapon);
    }
  }
}
