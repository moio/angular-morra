import { Injectable } from '@angular/core';
import {WeaponService} from './weapon.service';

@Injectable({
  providedIn: 'root'
})
export class WeaponHelperService {
  static weaponService: WeaponService = new WeaponService();

  static getRandomWeapon() {
    return WeaponHelperService.weaponService.getWeapon(
      Math.floor((Math.random() * WeaponHelperService.weaponService.weapons.length))
    );
  }
}
