
export enum Winner {
  Player1 = -1,
  Player2 = 1,
  Tie = 0
}
