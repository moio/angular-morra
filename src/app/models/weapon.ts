export enum Weapon {
  ROCK = 'rock',
  PAPER = 'paper',
  SCISSOR = 'scissor',
  LIZARD = 'lizard',
  SPOCK = 'spock',
  RANDOM = 'random',
}
