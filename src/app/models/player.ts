import {Weapon} from "./weapon";

export interface Player {
  weapon: Weapon;

  chooseWeapon(weapon: Weapon): void;
}
