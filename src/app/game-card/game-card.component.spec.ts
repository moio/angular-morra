import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Weapon } from '../models/weapon';

import { GameCardComponent } from './game-card.component';

describe('GameCardComponent', () => {
  let component: GameCardComponent;
  let fixture: ComponentFixture<GameCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GameCardComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should listen for weapon changes', () => {
    spyOn(component, 'onClick');
    let weaponElements = fixture.debugElement.queryAll(By.css('i'));
    weaponElements[0].nativeElement.click();

    expect(component.onClick).toHaveBeenCalled();
  });

  it('should emit on weapon changes', () => {
    spyOn(component.selectedWeaponChange, 'emit');
    component.onClick(Weapon.ROCK);
    expect(component.selectedWeaponChange.emit).toHaveBeenCalled();
  });
});
