import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Weapon } from '../models/weapon';
import { WeaponService } from '../services/weapon.service';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.css'],
})
export class GameCardComponent implements OnInit {
  weapons: Weapon[];
  @Output() selectedWeaponChange = new EventEmitter();
  @Input() selectedWeapon!: Weapon;

  constructor(private weaponService: WeaponService) {
    this.weapons = weaponService.weapons;
  }

  onClick(weapon: Weapon): void {
    this.selectedWeaponChange.emit(weapon);
  }

  ngOnInit(): void {}
}
