import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Weapon } from '../models/weapon';
import { GameService } from '../services/game.service';
import { PlayComponent } from './play.component';

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;
  let gameService: GameService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlayComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(GameService.prototype, 'play');
    gameService = fixture.debugElement.injector.get(GameService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('plays a round when player chose a weapon', () => {
    component.onSelected(Weapon.ROCK);
    expect(gameService.play).toHaveBeenCalled();
  });

  it('plays a round when player chose a random weapon', () => {
    component.randomPlay();
    expect(gameService.play).toHaveBeenCalled();
  });
});
