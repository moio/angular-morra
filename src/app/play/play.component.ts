import { Component, OnInit } from '@angular/core';
import { Player } from '../models/player';
import { Weapon } from '../models/weapon';
import { BotService } from '../services/bot.service';
import { GameService } from '../services/game.service';
import { PlayerService } from '../services/player.service';
import {WeaponHelperService} from '../services/weapon-helper.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css'],
})
export class PlayComponent implements OnInit {
  player!: Player;
  bot!: Player;

  constructor(public game: GameService) {
    this.player = new PlayerService();
    this.bot = new BotService();
  }

  ngOnInit(): void {}

  randomPlay() {
    this.onSelected(WeaponHelperService.getRandomWeapon());
  }

  onSelected(weapon: Weapon) {
    this.player.chooseWeapon(weapon);
    this.bot.chooseWeapon(Weapon.RANDOM);
    this.game.play(this.player, this.bot);
  }
}
